import { BaseClient } from '../helpers/base-client';
import { ActivitiesResponse } from '../models/activities';
import { SlugsResponse } from '../models/slugs';
import { musementAPIClient } from './musement-api-client';

type GetActivitySlug = (cityId: number) => Promise<SlugsResponse>;

export const slugService = musementAPIClient.extend<{
  getActivitySlug: GetActivitySlug;
}>({
  getActivitySlug: async ({ musementApiClient }, use) => {
    const getActivitySlug: GetActivitySlug = async (cityId) => {
      const activities: ActivitiesResponse =
        await musementApiClient.fetchActivitiesInCity(cityId);

      const httpClient = new BaseClient();
      const slugHost =
        process.env.SLUG_HOST || 'https://slug-service-cf.prod.musement.com';
      const slugs: SlugsResponse = await httpClient.fetchData(
        `${slugHost}/paths/activities/uuid/${activities.data[0].uuid}?no_redirect=true&locales=en-US`
      );
      return slugs;
    };

    await use(getActivitySlug);
  },
});
