import { test as base } from '@playwright/test';
import { MusementAPIClient } from '../helpers/musement-api-client';

export const musementAPIClient = base.extend<{
  musementApiClient: MusementAPIClient;
}>({
  musementApiClient: async ({}, use) => {
    await use(
      new MusementAPIClient(
        process.env.API_MUSEMENT_HOST || 'https://api.musement.com'
      )
    );
  },
});
