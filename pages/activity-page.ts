import { Locator, expect } from '@playwright/test';
import { ACTIVITY_PAGE_LOCATORS } from '../object-repository/locators/activity-page-locators';
import { BasePage } from './base-page';

export class ActivityPage extends BasePage {
  async open(url: string): Promise<void> {
    await this.page.goto(url);
  }

  async assertBreadcrumbsMatchWithSearchedActivityInThatCity(
    expectedBreadcrumbs: Array<string | RegExp>
  ): Promise<void> {
    const breadcrumbs: Locator = this.page
      .locator(ACTIVITY_PAGE_LOCATORS.BREADCRUMBS)
      .getByRole('link');

    await super.assertBreadcrumbs(breadcrumbs, expectedBreadcrumbs);
  }

  async assertPlaceInActivityMatchWithRequestedCity(
    city: string
  ): Promise<void> {
    await expect(this.page.locator(ACTIVITY_PAGE_LOCATORS.BLOCK).getByText(`Place: ${city}`)).toBeInViewport();
  }
}
