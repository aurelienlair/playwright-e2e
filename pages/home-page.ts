import { HOME_PAGE_LOCATORS } from '../object-repository/locators/home-page-locators';
import { BasePage } from './base-page';

export class HomePage extends BasePage {
  async open(): Promise<void> {
    await this.page.goto('/us/');
  }

  async clickOnSearchBar(): Promise<void> {
    await this.page.locator(HOME_PAGE_LOCATORS.CENTRAL_SEARCH_BAR).click();
  }

  async insertCity(cityName: string): Promise<void> {
    await this.page
      .locator(HOME_PAGE_LOCATORS.CENTRAL_SEARCH_BAR)
      .fill(cityName);
  }

  async clickSuggestedCity(cityName: string): Promise<void> {
    await this.page.getByText(cityName).click();
  }
}
