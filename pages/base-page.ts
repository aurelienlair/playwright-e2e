import { Page, Locator, expect } from '@playwright/test';
import { BASE_PAGE_LOCATORS } from '../object-repository/locators/base-page-locators';

export class BasePage {
  protected readonly page: Page;

  constructor(page: Page) {
    this.page = page;
  }

  async assertBreadcrumbs(
    breadcrumbs: Locator,
    expectedBreadcrumbs: (string | RegExp)[]
  ) {
    await expect(breadcrumbs).toHaveCount(expectedBreadcrumbs.length);
    const breadcrumbsLinks = await breadcrumbs.all();

    for (let index = 0; index < expectedBreadcrumbs.length; index++) {
      const breadcrumbLink = breadcrumbsLinks[index];
      await expect(breadcrumbLink).toHaveText(expectedBreadcrumbs[index]);
      await expect(breadcrumbLink).toBeInViewport();
    }
  }

  async acceptCookies(): Promise<void> {
    await this.page.locator(BASE_PAGE_LOCATORS.COOKIES_BANNER).click();
  }
}
