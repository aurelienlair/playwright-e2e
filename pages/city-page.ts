import { expect, type Locator } from '@playwright/test';
import {
  CITY_PAGE_LOCATORS,
} from '../object-repository/locators/city-page-locators';
import { BasePage } from './base-page';

export class CityPage extends BasePage {
  async assertBreadcrumbsMatchWithSearchedCity(expectedBreadcrumbs: Array<string | RegExp>): Promise<void> {
    const breadcrumbs: Locator = this.page
      .locator(CITY_PAGE_LOCATORS.BREADCRUMBS)
      .getByRole('link');

    await super.assertBreadcrumbs(breadcrumbs, expectedBreadcrumbs);
  }

  async assertHeadingsMatchWithSearchedCity(name: string): Promise<void> {
    await expect(
      this.page.getByRole('heading', { name, level: 1 })
    ).toBeInViewport();

    await expect(
      this.page
        .getByRole('heading', { name: 'recommend' })
        .filter({ hasText: name })
    ).toBeInViewport();
  }

  async containsSeveralExperiencesForSearchedCity(): Promise<void> {
    await expect(
      this.page.locator(CITY_PAGE_LOCATORS.EXPERIENCES_COUNTER)
    ).toHaveText(/[1-9][0-9]* Experience/);
  }
}
