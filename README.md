# Playwright E2E tests

This project is an experimental learning of [Playwright](https://playwright.dev) in order to become more familiar with the framework and its functionalities.
It use real use cases on [TUI Experiences](https://www.tuiexperiences.com/us) platform.

## System requirements

### Nodejs

This project needs [NPM scripts](https://docs.npmjs.com/cli/v7/using-npm/scripts) and [NodeJs](https://nodejs.org/en/) installed on your local machine.

## Getting Started

### Running tests locally

#### Commands

To install the dependencies

```bash
npm install
```

And then [install](https://playwright.dev/docs/browsers#install-browsers) Playwright supported browsers

```bash
npx playwright install
```

#### Environment variables

In order to properly work, the test suite needs the environment variables to be configured.

```bash
cp .env.dist .env
```

Then replace the values accordingly

#### Codegen

It is possible to record manual actions within a pages by using the [codegen](https://playwright.dev/docs/codegen-intro#running-codegen) command.

```bash
npx playwright codegen 'https://www.tuiexperiences.com/us/'
```

Every single action will be tracked as code, once finished just click on the "Record" button to save it.

<img src="./docs/images/codegen.png" alt="codegen" style="width:80%;"/>

#### Run test with debugger

It is possible to run the test suite with a [debugger](https://playwright.dev/docs/debug) so that every single step can be debugged. Below an example of how to run the test suite on Chromium with the debugger.

```bash
npx playwright test --debug
```

#### Run a specific test with grep

In command line it is possible to specify which test(s) to run by adding a [grep](https://playwright.dev/docs/api/class-testproject#test-project-grep) option.

```bash
npx playwright test --grep 'city page'
```

#### Run tests on a specific project

```bash
 npx playwright test --project 'Safari'
```

#### Help

```bash
npx playwright --help
npx playwright test --help
```

### Running tests in Gitlab pipeline

Whenever a commit is pushed, a pipeline is triggered running the whole test suite in [headless mode](https://en.wikipedia.org/wiki/Headless_browser).
At the end of the pipeline it generated a report using [Allure](https://qameta.io/allure-report) including the history.

<img src="./docs/images/pipeline.png" alt="pipeline" style="width:80%;"/>

#### Reporting

To see the outcome of the whole test suite with Allure reports open <https://aurelienlair.gitlab.io/playwright-e2e/>

The reporting is divided cross [Playwright projects](https://playwright.dev/docs/test-projects#configure-projects-for-multiple-browsers) and you can see the subdivision leveraging [Playwright steps](https://github.com/allure-framework/allure-js/blob/master/packages/allure-playwright/README.md#steps-usage)

<img src="./docs/images/allure_report.png" alt="allure_report" style="width:80%;"/>

## Git commit message convention

This projects follows the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

```shell
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

Example:

```shell
docs: add description of TypeScript run command
```

| Type | Description |
|------| ----------- |
| `style` | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc) |
| `build` | Changes to the build process |
| `chore` | Changes to the build process or auxiliary tools and libraries such as documentation generation |
| `docs` | Documentation updates |
| `feat` | New features |
| `fix`  | Bug fixes |
| `refactor` | Code refactoring |
| `test` | Adding missing tests |
| `perf` | A code change that improves performance |

## Useful links

* [Playwright](https://playwright.dev)
* Playwright [CI/CD](https://playwright.dev/docs/ci#gitlab-ci)
* Playwright Allure [plugin](https://www.npmjs.com/package/allure-playwright)
* [Object repository](https://www.browserstack.com/guide/object-repository-in-selenium#:~:text=in%20Selenium%20Script-,What%20is%20an%20Object%20Repository%20in%20Selenium%3F,properties%20in%20Selenium.) pattern
* [Page Object Model with Playwright](https://andrewbayd.medium.com/page-object-model-with-playwright-f8fbd5e8fa0f)