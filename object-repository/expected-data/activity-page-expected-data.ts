export const EXPECTED_ACTIVITY_PAGE_BREADCRUMBS = [
  'Home',
  'France',
  /Paris/,
  /.+/,
];
export const EXPECTED_CITY_PLACE = 'Paris';
