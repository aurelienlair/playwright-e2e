export const ACTIVITY_PAGE_LOCATORS = {
  BREADCRUMBS: '[data-testid="activity_page_breadcrumbs"]',
  BLOCK: '[data-test="activity_page"]',
};
