export const CITY_PAGE_LOCATORS = {
  BREADCRUMBS: '[data-testid="city_page_breadcrumbs"]',
  EXPERIENCES_COUNTER: '[data-test-id="activity-count-text"]',
};
