export const HOME_PAGE_LOCATORS = {
  PAGE_HEADER_TITLE: '[data-test-id="page-header-title"]',
  CENTRAL_SEARCH_BAR: '[data-testid="search-component-central-search-body"]',
};
