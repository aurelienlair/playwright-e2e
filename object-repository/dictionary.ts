type City = {
  id: number;
  name: string;
};

const cities: { [key: string]: City } = {
  PARIS: { id: 40, name: 'Paris' },
};

export const city = cities.PARIS;
