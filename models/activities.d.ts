export interface Meta {
  count: number;
  match_type: string;
  match_names: string[];
  match_ids: string[];
}
export interface Country {
  id: number;
  name: string;
  iso_code: string;
}
export interface City {
  id: number;
  name: string;
  country: Country;
  cover_image_url: string;
  url: string;
  time_zone: string;
}
export interface Durationrange {
  min: string;
  max: string;
}
export interface Language {
  code: string;
  name: string;
}
export interface Servicefee {
  currency: string;
  value: number;
  formatted_value: string;
  formatted_iso_value: string;
}
export interface Category {
  id: number;
  name: string;
  level: string;
  code: string;
  event_image_url: string;
  cover_image_url: string;
  url: string;
  parent_id?: number;
}
export interface Reviewsaggregatedinfo {
  '1': number;
  '2': number;
  '3': number;
  '4': number;
  '5': number;
}
export interface Flavour {
  id: number;
  name: string;
  active: boolean;
  slug: string;
}
export interface Vertical {
  id: number;
  name: string;
  active: boolean;
  code: string;
  slug: string;
  url: string;
  meta_title: string;
  meta_description: string;
  cover_image_url: string;
  relevance: number;
}
export interface Supplier {}
export interface Datum {
  slug_id: string;
  max_confirmation_time: string;
  cutoff_time: string;
  booking_type: string;
  uuid: string;
  city: City;
  saves: number;
  title: string;
  relevance: number;
  emergency_phone_number: string;
  relevance_venue: number;
  must_see: boolean;
  last_chance: boolean;
  top_seller: boolean;
  voucher_access_usage: string;
  temporary: boolean;
  description: string;
  about: string;
  meeting_point: string;
  duration: string;
  duration_range: Durationrange;
  validity: string;
  has_price_info_on_date: boolean;
  open: boolean;
  ticket_not_included: boolean;
  likely_to_sell_out: boolean;
  special_offer: boolean;
  exclusive: boolean;
  best_price: boolean;
  daily: boolean;
  languages: Language[];
  group_size: Language[];
  food: any[];
  services: any[];
  features: Language[];
  highlights: string[];
  included: string[];
  not_included: string[];
  is_available_today: boolean;
  is_available_tomorrow: boolean;
  cover_image_url: string;
  service_fee: Servicefee;
  retail_price: Servicefee;
  retail_price_without_service_fee: Servicefee;
  original_retail_price_without_service_fee: Servicefee;
  original_retail_price: Servicefee;
  discount: number;
  categories: Category[];
  reviews_number: number;
  reviews_avg: number;
  reviews_aggregated_info: Reviewsaggregatedinfo;
  latitude: number;
  longitude: number;
  url: string;
  flavours: Flavour[];
  verticals: Vertical[];
  supplier: Supplier;
  giftable: boolean;
  has_passenger_info: boolean;
  has_extra_customer_data: boolean;
  buy_multiplier: number;
  ticket: boolean;
  free_cancellation: boolean;
}
export interface ActivitiesResponse {
  meta: Meta;
  data: Datum[];
}
