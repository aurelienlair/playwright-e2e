export interface SlugsResponse {
  resource_type: string;
  is_redirect: boolean;
  identifiers: Identifiers;
  slugs?: Slug[];
  paths: Path[];
}

export interface Identifiers {
  id: number;
  uuid: string;
  tiny_id: string;
}

export interface Slug {
  slug: string;
  locale: string;
}

export interface Path {
  path: string;
  locale: string;
}
