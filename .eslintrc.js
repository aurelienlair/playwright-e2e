module.exports = {
  env: {
    node: true,
  },
  extends: ['plugin:@musement/library'],
  plugins: ['prettier'],
  rules: {
    camelcase: [2, { properties: 'always' }],
    'require-await': 'error',
    'no-shadow': 'warn',
    'no-use-before-define': 'error',
    'no-var': 'error',
    'prefer-template': 'warn',
    'object-shorthand': 'warn',
    'prefer-const': 'error',
    'prefer-rest-params': 'error',
  },
};
