import fetch from 'node-fetch';
export type Headers = Record<string, string>;
export class BaseClient {
  protected additionalHeaders?: Headers;
  public constructor(additionalHeaders?: Headers) {
    this.additionalHeaders = additionalHeaders;
  }
  public async fetchData(url: string) {
    const headers = {
      'Accept-Language': 'en-US',
      Accept: 'application/json',
      ...this.additionalHeaders,
    };
    const response = await fetch(url, { headers });
    const data = await response.json();

    return data;
  }
}
