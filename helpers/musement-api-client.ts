import { ActivitiesResponse } from '../models/activities';
import { BaseClient, Headers } from './base-client';

export class MusementAPIClient extends BaseClient {
  private baseUrl: string;
  public constructor(baseUrl: string, additionalHeaders?: Headers) {
    if (!process.env.X_MUSEMENT_APPLICATION_HEADER) {
      throw Error(
        'X_MUSEMENT_APPLICATION_HEADER is not defined among the environment variables'
      );
    }

    super({
      'X-Musement-Version': '3.4.0',
      'X-Musement-Application': process.env.X_MUSEMENT_APPLICATION_HEADER,
      'X-Musement-Market': 'us-te',
      ...additionalHeaders,
    });

    this.baseUrl = baseUrl;
  }

  public async fetchActivitiesInCity(
    cityId: number,
    limit: number = 1
  ): Promise<ActivitiesResponse> {
    return await this.fetchData(
      `${this.baseUrl}/api/v3/activities?city_in=${cityId}&limit=${limit}`
    );
  }
}
