import { ActivityPage } from '../pages/activity-page';
import {
  EXPECTED_ACTIVITY_PAGE_BREADCRUMBS,
  EXPECTED_CITY_PLACE,
} from '../object-repository/expected-data/activity-page-expected-data';
import { city } from '../object-repository/dictionary';
import { slugService } from '../fixtures/slug-service';
import { test as base } from '@playwright/test';

slugService(
  'From a city page to the activity page',
  async ({ page, getActivitySlug }) => {
    const slugs = await getActivitySlug(city.id);
    const activityPage = new ActivityPage(page);
    await base.step(
      `Open tuiexperiences activity page in ${EXPECTED_CITY_PLACE} and accept cookies`,
      async () => {
        await activityPage.open(slugs.paths[0].path);
        await activityPage.acceptCookies();
      }
    );

    await base.step(
      `Expect activity in Paris to be correctly loaded`,
      async () => {
        await activityPage.assertBreadcrumbsMatchWithSearchedActivityInThatCity(
          EXPECTED_ACTIVITY_PAGE_BREADCRUMBS
        );
        await activityPage.assertPlaceInActivityMatchWithRequestedCity(
          EXPECTED_CITY_PLACE
        );
      }
    );
  }
);
