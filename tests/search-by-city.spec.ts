import { test } from '@playwright/test';
import { HomePage } from '../pages/home-page';
import { EXPECTED_SUGGESTED_CITY } from '../object-repository/expected-data/home-page-expected-data';
import { CityPage } from '../pages/city-page';
import {
  EXPECTED_CITY_PAGE_BREADCRUMBS,
  EXPECTED_CITY_HEADING,
} from '../object-repository/expected-data/city-page-expected-data';
import { CITY } from '../object-repository/input-data/home-page-input-data';

test('Search for a city on the home page by clicking on the search button', async ({
  page,
}) => {
  const homePage = new HomePage(page);
  await test.step(
    'Open tuiexperiences homepage and accept cookies',
    async () => {
      await homePage.open();
      await homePage.acceptCookies();
    }
  );

  await test.step(
    `Expect to click on the search box, to fill ${CITY} and to click on "${EXPECTED_SUGGESTED_CITY}" suggestion`,
    async () => {
      await homePage.clickOnSearchBar();
      await homePage.insertCity(CITY);
      await homePage.clickSuggestedCity(EXPECTED_SUGGESTED_CITY);
    }
  );

  const activityPage = new CityPage(page);
  await test.step(
    `Expect ${CITY} city page to be correctly loaded`,
    async () => {
      await activityPage.assertBreadcrumbsMatchWithSearchedCity(
        EXPECTED_CITY_PAGE_BREADCRUMBS
      );
      await activityPage.assertHeadingsMatchWithSearchedCity(
        EXPECTED_CITY_HEADING
      );
      await activityPage.containsSeveralExperiencesForSearchedCity();
    }
  );
});
